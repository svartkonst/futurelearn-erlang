-module(third).
-export([fib/1, cuts/1, fib_tr/1, perfect/1]).

fib(0) -> 0;
fib(1) -> 1; 
fib(N) -> fib(N-1) + fib(N-2).


cuts(0) -> 1;
cuts(N) -> cuts(N-1) + N.

fib_tr(Target) -> fib_tr(Target, 0, 1).

fib_tr(0, N1, _N2) -> 
  N1;
fib_tr(Target, N1, N2) when Target > 0 -> 
  fib_tr(Target - 1, N2, N1 + N2).

% lol vi kollar aldrig om D är en divisor :-)
perfect(N) -> perfect(N, 1, 0).
perfect(N, _, N) -> true;             % if the sum of divisors equals N, it's perfect
perfect(N, _, S) when S > N -> false; % if the sum of divisors is larger than N, it's imperfect
perfect(N, D, S) when N rem D == 0 -> perfect(N, D+1, S+D);
perfect(N, D, S) -> 
  perfect(N, D+1, S).
