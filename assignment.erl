-module(assignment).
-export([perimeter/1, area/1, bits/1]).

%% Shape definitions
% {circle,    {Xcenter, Ycenter}, Radius}
% {rectangle, {Xcenter, Ycenter}, Width, Height}
% {square,    {Xcenter, Ycenter}, Length}
% {triangle,  {Xorigin, Yorigin}, V0, V1}
%   note that vectors don't have a center point, but originate from a corner.
%   triangles are defined byt two vectors, V0 and V1 that originate from {0,0}.


perimeter({circle, _, R}) -> R * math:pi();
perimeter({rectangle, _, W, H}) -> (W + H) * 2;
perimeter({square, _, L}) -> L * 4;

% See sides/2 below for further details. 
perimeter({triangle, _, V, U}) -> 
  {A,B,C} = sides(V, U),
  A + B + C.

area({circle, _, R})       -> R * R * math:pi();
area({rectangle, _, W, H}) -> W * H;
area({square, _, L})       -> L * L;
area({triangle, _, V, U}) ->
  {A,B,C} = sides(V, U),
  S = (A+B+C) / 2,
  S * (S-A) * (S-B) * (S-C).

% For a given number N, find the number of bits B that need to be set. 
% E.g. 
%   7 = 3,  because 0111      (1+2+4).
%   8 = 1,  because 1000      (8)
%   16 = 1, because 0001 0000 (16)
%   27 = 3, because 0001 1011 (16 + 8 + 2 + 1)
% 
% We do this by first finding an exponent E, such that 2^E > N.
% Once we have our ceiling, we work our way down again, exhausting N as we go.
% 
% If N is equal to either 2^E or 1, we can return B+1,
% otherwise we'll return B once N reaches 0.
bits(N) -> bits(N, 1, 0).

% In order to find the number of bits set, 
% we work up to the highest bit, then start exhausting N.

bits(0, _, B) -> B;         
bits(1, _, B) -> B + 1;     
bits(N, N, B) -> B + 1;

% If N is greater than P, double P and try again.
bits(N, P, B) when N > P -> 
  bits(N, P * 2, B);

% If N is less than P, we can start exhausting N 
% by working our way back down towards 0.
bits(N, P, B) when N < P ->
  Q = P div 2,
  bits(N-Q, Q, B+1).


%%% Utility/helper functions below. 

% hypotenuse(A,B) from an earlier assignment, included here for simplicity
hypotenuse(A,B) -> 
  C = (A*A) + (B*B),
  math:sqrt(C).

% Return the length of the sides of a triangle formed by the 
% two vectors AB and AC, and the implied third vector BC.
sides({Xa, Ya}, {Xb, Yb}) -> 
  A = hypotenuse(Xa, Ya),
  B = hypotenuse(Xb, Yb),
  C = hypotenuse(abs(Xa - Xb), abs(Ya - Yb)),
  {A,B,C}.