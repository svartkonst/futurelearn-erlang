-module(second).
-export([hypotenuse/2, perimeter/2, area/2, max/3,howManyEqual/3]).

hypotenuse(A,B) -> 
  C = first:square(A) + first:square(B),
  math:sqrt(C).

perimeter(A,B) -> 
  C = hypotenuse(A, B), 
  A + B + C.

area(A,B) -> 
  C = hypotenuse(A, B), 
  first:area(A, B, C).

max(A, B, C) ->
  Mab = max(A,B),
  max(Mab, C).

howManyEqual(A, B, C) -> howManyEqual([A, B, C],0).

howManyEqual([], M) -> M;
howManyEqual([H|T], M) -> 
  Bs = lists:filter(
    fun (A) -> A =/= H end, T),
  N = length(T) - length(Bs),
  howManyEqual(T, M + N).